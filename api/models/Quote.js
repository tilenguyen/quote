/**
 * Quote.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  schema: true,
  tableName: 'quote',
  attributes: {
    content: {
      type: 'string',
      required: true,
      columnName: 'content'
    },
    writter: {
      model: 'writter',
      defaultsTo: 'No Name'
    },
    language: {
      model: 'language'
    },
    toJSON: function(){
      return this.toObject();
    }
  }
};
