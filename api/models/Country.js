/**
 * Country.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  schema: true,
  tableName: 'country',
  attributes: {
    name: {
      type: 'string',
      columnName: 'name'
    },
    /* A Country have many languages */
    languages: {
      collection: 'language',
      via: 'countries',
      dominant: true
    },
    toJSON: function(){
      return this.toObject();
    }
  }
};
