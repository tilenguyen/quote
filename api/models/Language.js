/**
 * Language.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  schema: true,
  tableName: 'language',
  attributes: {
    shortName: { // abbreviation
      type: 'string',
      required: true,
      unique: true,
      columnName: 'shortName'
    },
    name: {
      type: 'string',
      columnName: 'name'
    },
    /* A language belong to many countries */
    countries: {
      collection: 'country',
      via: 'languages'
    },
    toJSON: function(){
      return this.toObject();
    }
  }
};
