/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

// var bcrypt = require('bcrypt');

module.exports = {
  schema: true,
  tableName: 'user',
  attributes: {
    name: {
      type: 'string',
      columnName: 'userName'
    },
    email: {
      type: 'email',
      unique: true,
      required: true,
      columnName: 'userEmail'
    },
    password: {
      type: 'string',
      columnName: 'userPassword',
      size: 6
    },
    toJSON: function(){
      var obj = this.toObject();
      delete obj.password;
      return obj;
    },
    getName: function(){
      return this.name;
    }
  },
  beforeCreate: function(values, cb) {
    // Encrypt password before create user.
    bcrypt.hash (values.password, 15, function(err, hash){
      if (err) return cb(err);
      values.password = hash;
      // Call callBack
      cb();
    })
  }
};
