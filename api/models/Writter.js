/**
 * Writter.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  schema: true,
  tableName: 'writter',
  attributes: {
    name: {
      type: 'string',
      required: true,
      columnName: 'name'
    },
    writterInfo: {
      type: 'string',
      columnName: 'writterInfo'
    },
    /* A Writter belong a Country */
    country: {
      model: 'country'
    },
    toJSON: function(){
      return this.toObject();
    }
  }
};
