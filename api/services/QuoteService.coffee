##
# Create by TILE
##
module.exports =
  addQuote: (data, cb)->
    Quote.create data, (err, db) ->
      if err
        return cb(err)
      return cb(db)
  getQuotes: (cb) ->
    Quote.native (err, collection) ->
      if err
        return cb ErrorService.SYSTEM(err)
      # Get a collection quote random
      collection.aggregate [{$sample: {size: 1}}]
        .toArray (err, quote) ->
          if err
            return cb ErrorService.SYSTEM(err)
          return cb {code: 200, data: quote}
