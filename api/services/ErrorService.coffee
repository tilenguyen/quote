##
# Create by TILE
##

SYSTEM_ERROR_CODE = 'SYSTEM'

ERROR_STATUS =
  SYSTEM: 'Error system'
  MISSING_PARAM_CONTENT: "Missing param content"
  MISSING_DATA: "Missing param data"



_ = require 'lodash'
_(ERROR_STATUS).forEach (message, code) ->
  if code is SYSTEM_ERROR_CODE
    exports[code] = (err) ->
      if err && err.code && err.error
        err
      if err
        sails.log.error err
      message = ERROR_STATUS[SYSTEM_ERROR_CODE]
      result = code: SYSTEM_ERROR_CODE, error: message
  else
    exports[code] = code: code, message: message
