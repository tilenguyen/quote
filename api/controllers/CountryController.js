/**
 * CountryController
 *
 * @description :: Server-side logic for managing Countries
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var _ = require("lodash");
var async = require("async");
var ObjectId = require('mongodb').ObjectID;

module.exports = {
	init: function(req, res){
		var data = req.param('data');
		if (!data || data.length <= 0){
			return res.badRequest(ErrorService.MISSING_DATA);
		}
		// loop data request.
		async.eachSeries(data, function interatee1(value, callback){
			function createCountry(d, cb){
				Country.native(function(err, coll){
					if (err) return res.forbidden(ErrorService.SYSTEM(err));
					coll.insert(d, function(err, coun){
						if (err) return res.forbidden(ErrorService.SYSTEM(err));
						async.eachSeries(coun.ops[0].languages, function result(v, callB){
							Language.native(function(err, collec){
									if (!err){
										collec.find({_id: ObjectId(v)}).toArray(function(er, result){
											if (!er){
												if (result[0].countries && result[0].countries.indexOf(coun.ops[0]._id) == -1){
													result[0].countries.push(coun.ops[0]._id);
												} else if (!result[0].countries){
													result[0].countries = [];
													result[0].countries.push(coun.ops[0]._id);
												}
												collec.save(result[0]);
											}
										});
									}
									callB();
							});
						}, function done(){
							cb();
						});
					});
				});
			};

			var d = {name: value.country};
			if (value.languages.length > 0){
				Language.native(function(err, collection){
					if (err) return res.forbidden(ErrorService.SYSTEM(err));
					collection.find({
						shortName: {$in: value.languages}
					}).toArray(function(err, languages){
						if (err) return res.forbidden(ErrorService.SYSTEM(err));
						d.languages = _.map(languages, '_id');
						createCountry(d, callback);
					});
				});
			} else{
				createCountry(d, callback);
			}

		}, function done(){
			return res.ok({success: "ok"});
		});
	},
	update: function(req, res){

	}
};
