/**
 * LanguageController
 *
 * @description :: Server-side logic for managing Languages
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var _ = require('lodash');

module.exports = {
	init: function (req, res) {
		var data = req.param('data');
		if (!data || data.length <= 0){
			return res.badRequest();
		}
		// _.forEach(function(value){
		//
		// })
		for(var l of data){
			var d = {
				shortName: l[0],
				name: l[1]
			};
			sails.log.info(JSON.stringify(d));
			Language.create(d, function(err, language){
				if (err){
					sails.log.warn(JSON.stringify(err))
				}
				return;
			})
		}
	},
	update: function (req, res){

	}
};
