/**
 * QuoteController
 *
 * @description :: Server-side logic for managing Quotes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	addQuote: function (req, res){
		var data = req.allParams();
		sails.log.info(data);
		QuoteService.addQuote(data, function(db){
			sails.log.info (db);
			// if(db.code) return res.badRequest();
			res.ok(db);
		})
	},
	getQuoteById: function (req, res){

	},
	getQuotes: function (req, res){
		QuoteService.getQuotes(function(cb){
			res.ok(cb.data);
		});
	},
	updateQuoteById: function (req, res){

	},
	deleteQuoteById: function (req, res){

	}
};
