/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	createUser: function (req, res){

	},
	getUserById: function (req, res){

	},
	getUsers: function (req, res){

	},
	updateUserById: function (req, res){

	},
	deleteUserById: function (req, res){

	}
};
