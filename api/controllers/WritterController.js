/**
 * WritterController
 *
 * @description :: Server-side logic for managing Writters
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	create: function (req, res){

	},
	getWritterById: function (req, res){

	},
	getWritters: function (req, res){

	},
	updateWritterById: function (req, res){

	},
	deleteWritterById: function (req, res){

	}
};
