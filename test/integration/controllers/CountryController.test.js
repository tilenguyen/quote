/**
	@author TILE
*/

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = 'http://localhost:1338';
var url = '/country';
var countries = require('../../fixture/countries.json');

chai.use(chaiHttp);

describe('---- Test Country controller ----', function(){
	describe('-- Add countries default (run only once) --', function(){
		it('should be add countries ok', function(done){
			chai.request(server).post(url)
				.set({}).send({data: countries}).end(function(err, res){
					console.log("--- ERROR ---");
					console.log(JSON.stringify(err));
					console.log("--- RESULT ---");
					console.log(JSON.stringify(res));
					return done();
				});
		})
	});
});
