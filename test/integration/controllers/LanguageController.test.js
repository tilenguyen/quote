/**
	@author TILE
*/

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = 'http://localhost:1338';
var url = '/language';
var language = require('../../fixture/language.json');

chai.use(chaiHttp);


describe('---- Test language controller ----', function(){
	describe('-- Add languages default (run only once) --', function(){
		it('should be add language ok', function(done){
			chai.request(server).post(url)
				.set({}).send({data: language}).end(function(err, res){
					console.log("--- ERROR ---");
					console.log(JSON.stringify(err));
					console.log("--- RESULT ---");
					console.log(JSON.stringify(res));
					return done();
				});
		})
	});
});
