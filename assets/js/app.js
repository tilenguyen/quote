'use strict';

var quoteApp = angular.module('quoteApp', ['ngRoute', 'ui.bootstrap']);
quoteApp.config(['$routeProvider',
function ($routeProvider){
  $routeProvider.when('/', {
    templateUrl: '/templates/quote.html',
    controller: 'QuoteCtrl'
  }).otherwise({
    redirectTo: '/',
    caseInsensitiveMatch: true
  })
}]);
quoteApp.controller('QuoteCtrl',['$scope', '$rootScope', 'QuoteService', function ($scope, $rootScope, QuoteService){
  $scope.formData = {};
  $scope.quotes = [];
  console.log("get Controller of angular");
  QuoteService.getQuotes().then(function(responses){
    console.log(responses);
    $scope.quotes = responses;
  });

  $scope.addQuote = function(){
    QuoteService.addQuote($scope.formData).then(function(responses){
      $scope.quotes.push($scope.formData);
      $scope.formData = {};
    });
  };

  $scope.removeQuote = function(quote){
    QuoteService.removeQuote(quote).then(function(responses){
      $scope.quotes.splice($scope.quotes.indexOf(quote), 1);
    });
  };
}])
