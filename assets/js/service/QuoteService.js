/**
*  Create by TILE
**/


/** quoteApp is a variable of angularJS */
quoteApp.service('QuoteService', function($http, $q){
  return {
    'getQuotes': function(){
      var defer = $q.defer();
      $http.get('/quotes')
      .success(function(resp){
        defer.resolve(resp);
      }).error(function(err){
        defer.reject(err);
      });
      return defer.promise;
    },
    'addQuote': function(quote){
      var defer = $q.defer();
      $http.post('/quotes', quote)
      .success(function(resp){
        defer.resolve(resp);
      }).error(function(err){
        defer.reject(err);
      });
      return defer.promise;
    },
    'removeQuote': function(quote){
      var defer = $q.defer();
      $http.delete('/removeQuote', quote)
      .success(function(resp){
        defer.resolve(resp);
      }).error(function(err){
        defer.reject(err);
      });
      return defer.promise;
    }
  };
})
